/**
 * @format
 */

 import {AppRegistry, YellowBox} from 'react-native';
 import App from './src/app/App';
 import {name as appName} from './app.json';
 
 YellowBox.ignoreWarnings(["Warning:", "Module RCTImageLoader", 
   "RCTRootView cancelTouches", "VirtualizedLists", "Calling bridge.imageLoader"]);
 
 AppRegistry.registerComponent(appName, () => App);
