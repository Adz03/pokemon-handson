/* eslint-disable */
import React from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';
import Color from "__color";
import Home from "../modules/home"

const AppNavigator = createStackNavigator({
	Home: {
		screen: Home,
		navigationOptions: {
			title: "Pokemon"
		}
	},
},{
	mode: "",
	defaultNavigationOptions: {
		headerStyle: {
      backgroundColor: '#FFF700',
    },
		headerTintColor: Color.black,
    headerBackTitle: " ",
		headerTitleAlign: 'center',
		gestureEnabled: false,
		...TransitionPresets.SlideFromRightIOS,
	},
});

const AppContainer = createAppContainer(AppNavigator);

const App = (props) => {
	return (
		<AppContainer />
	)
};

export default App;

