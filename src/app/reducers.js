/* eslint-disable */
import { combineReducers } from "redux";

import { home } from "../modules/home";

const reducer = combineReducers({
	home,
});

const rootReducer = (state = {}, action) => {
	switch (action.type) {
	
	case "logout/types/LOGOUT": {
		const newState = { ...state };

		return reducer(newState, action);
	}
	default:
		return reducer(state, action);
	}
};

export default rootReducer;
