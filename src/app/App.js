/* eslint-disable */
import React, {Component} from "react";
import Loading from "__src/components/Loading";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "remote-redux-devtools";
import {createLogger} from "redux-logger";
import * as reduxStorage from "redux-storage";
import debounce from "redux-storage-decorator-debounce";
import createEngine from "./LocalStorage";
import AppReducer from "./reducers";
import AppNavigator from "./navigator";
const logger = createLogger({});
const engine = debounce(createEngine("pokemon-key"), 1500);
const storage = reduxStorage.createMiddleware(engine);
const composeEnhancers = composeWithDevTools({ realtime: true });
const store = createStore(reduxStorage.reducer(AppReducer),
	composeEnhancers(applyMiddleware(thunk, logger, storage))
);

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			rehydrated: false
		};
	}

	componentDidMount() {
		// AsyncStorage.clear();
		reduxStorage.createLoader(engine)(store)
			.then((state) => {
				console.debug("Loaded previous state: ", state);
				this.setState({
					rehydrated: true,
				});
			})
			.catch((e) => {
				console.log("Unable to restore previous state!", e);
			});
	}

	render(){
		if (!this.state.rehydrated) {
			return (
				<Loading />
			);
		}
		return (
			<Provider store={store}>
				<AppNavigator />
			</Provider>
		);
	}
}
