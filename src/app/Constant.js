
const screen = {
  HOMESCREEN: "HOMESCREEN",
  ACCOUNTSCREEN: "ACCOUNTSCREEN",
  WALLETSCREEN: "WALLETSCREEN",
  GOCOURIERSCREEN: "GOCOURIERSCREEN",
  BOOKINGSCREEN: "BOOKINGSCREEN",
  LOGINSCREEN: "LOGINSCREEN",
	url: "http://localhost:80/v1",
}

export default screen;