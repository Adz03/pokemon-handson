
const API = {
	get: async (url) => {
		try {
			const response = await fetch(url);

			if (response.ok === false){
				throw await response.json();
			}

			return response.json().catch(() => {
				return {message: "Success"};
			});
		} catch (e) {
			console.log("get", JSON.stringify(e));
			throw e;
		}
	},
};

export default API;
