export const LOGOUT = "logout/types/LOGOUT";


export const GET_POKEMON_LOAD = "home/types/GET_POKEMON_LOAD";
export const GET_POKEMON = "home/types/GET_POKEMON";
export const GET_POKEMON_FAILED = "home/types/GET_POKEMON_FAILED";

export const GET_MORE_POKEMON_LOAD = "home/types/GET_MORE_POKEMON_LOAD";
export const GET_MORE_POKEMON = "home/types/GET_MORE_POKEMON";
export const GET_MORE_POKEMON_FAILED = "home/types/GET_MORE_POKEMON_FAILED";
