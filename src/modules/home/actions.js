/* eslint-disable no-throw-literal */
import * as Types from "./types";
import API from "__src/api";

export const getPokemon = () => (
	async (dispatch) => {
		try {
			dispatch({ type: Types.GET_POKEMON_LOAD });
			const url = "https://pokeapi.co/api/v2/pokemon?limit=25&offset=0"

			const result = await API.get(url);

			dispatch({ type: Types.GET_POKEMON, data: result.results });
		} catch (err) {
			dispatch({ type: Types.GET_POKEMON_FAILED, error: err });
		}
	}
);

export const loadMorePokemon = (limit) => (
	async (dispatch) => {
		try {
			dispatch({ type: Types.GET_MORE_POKEMON_LOAD });
			const url = `https://pokeapi.co/api/v2/pokemon?limit=25&offset=${limit}`

			const result = await API.get(url);

			dispatch({ type: Types.GET_MORE_POKEMON, data: result.results });
		} catch (err) {
			dispatch({ type: Types.GET_MORE_POKEMON_FAILED, error: err });
		}
	}
);
