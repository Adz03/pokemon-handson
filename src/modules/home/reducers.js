import { combineReducers } from "redux";
import * as Types from "./types";
import _ from "lodash";

const isGettingPokemon = (state = false, action) => {
	switch (action.type){
	case Types.GET_POKEMON_LOAD:
		return true;
	case Types.GET_POKEMON:
	case Types.GET_POKEMON_FAILED:
		return false;
	default:
		return state;
	}
};

const pokemonList = (state = [], action) => {
	switch (action.type){
	case Types.GET_POKEMON:
		return action.data;

	case Types.GET_MORE_POKEMON:
		let newState = [...state];

		return _.concat(newState, action.data);;
	case Types.GET_POKEMON_LOAD:
	case Types.GET_POKEMON_FAILED:
		return [];
	default:
		return state;
	}
};

const pokemonFailed = (state = "", action) => {
	switch (action.type){
	case Types.GET_POKEMON_FAILED:
	case Types.GET_MORE_POKEMON_FAILED:
		return action.error;
	case Types.GET_POKEMON_LOAD:
	case Types.GET_MORE_POKEMON_LOAD:
	case Types.GET_POKEMON:
		return "";
	default:
		return state;
	}
};

const isLoadMorePokemon = (state = false, action) => {
	switch (action.type){
	case Types.GET_MORE_POKEMON_LOAD:
		return true;
	case Types.GET_MORE_POKEMON:
	case Types.GET_MORE_POKEMON_FAILED:
		return false;
	default:
		return state;
	}
};

const loadMoreFailed = (state = "", action) => {
	switch (action.type){
	case Types.GET_MORE_POKEMON_FAILED:
		return action.error;
	case Types.GET_MORE_POKEMON_LOAD:
	case Types.GET_MORE_POKEMON:
		return "";
	default:
		return state;
	}
};

export default combineReducers({
	isGettingPokemon,
	pokemonList,
	pokemonFailed,

	isLoadMorePokemon,
	loadMoreFailed
});
