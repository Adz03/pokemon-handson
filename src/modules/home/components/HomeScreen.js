/* eslint-disable */
import React from "react";
import {SafeAreaView, View, Text, StyleSheet, FlatList} from "react-native";
import DropDownItem from "__src/components/DropDownItem";
import DropDown from "__src/components/DropDown";
import Field from "__src/components/Field";
import {Icon, Avatar} from "react-native-elements";
import Loading from "__src/components/Loading";
import Color from "__color";
import _ from "lodash";

class HomeScreen extends React.PureComponent {
	state = {
		selected: "",
		error: {}
	}

	componentDidMount(){
		const {actions} = this.props;

		actions.getPokemon();
	}

	onLoadMore = () => {
		const {actions, home: {pokemonList}} = this.props;

		actions.loadMorePokemon(pokemonList.length);
	}

	onChangeText = (val) => {
		this.setState({selected: val});
		// this.renderData();
	}

	renderBase = () => {
    const {selected, error} = this.state;
    const idName = selected.idType ? selected.idType.name : "";

    return (
      <Field label="Choose"
        labelStyle={styles.labelStyle} disable
        value={selected} 
        icon="expand"
        bordStyle={{borderBottomWidth: 0.5}}
        onChangeText={this.onChangeText} />
    )
  }

	renderRow( rowData, rowID, highlighted) {
    return (
      <DropDownItem row {...{rowData, highlighted}}/>
		);
  }

	renderItem = ({item, index}) => {
		return (
			<View key={`${index}`} activeOpacity={0.8} style={styles.rowStyle} >
				{/* <View style={styles.rowView}/> */}
				<Avatar
					rounded
					size="medium"
					source={{
						uri:item.url,
					}}
				/>
				{/* <Image style={styles.sectionImage} source={{uri: item.url}}  /> */}
				<Text style={styles.rowText}>{item.name}</Text>
			</View>
		)
	};

	renderData = () => {
		const {home: {pokemonList}} = this.props;
		const { selected } = this.state;

		switch(selected){
			case "Default":
				return _.slice(pokemonList, 0, 25);
			case "Upgrade":
				return _.map(pokemonList, item => {
					return {
						name: `${item.name} Upgraded`,
						url: item.url
					};
				});
			case "Imposters":
				return _.filter(pokemonList, item => {
					return item.name.toUpperCase().startsWith('A');
				});
			case "Ultimate":
				let newPokemon = _.map(pokemonList, item => {
					return {
						name: `${item.name} Upgraded`,
						url: item.url
					};
				});
				return _.slice(newPokemon, 0, 20);
			default:
				return pokemonList;
		}
	}

	render() {
		const { home: {isGettingPokemon, pokemonList, isLoadMorePokemon}} = this.props;
		const { selected } = this.state;

		return (
			<SafeAreaView style={styles.container}>
				<View style={[styles.container, { paddingHorizontal: 20}]}>
				<DropDown
					animated={false}
					renderBase={this.renderBase.bind(this)}
					dropdownStyle={styles.dropdownstyle}
					options={["Default", "Upgrade", "Imposters","Ultimate"]}
					onSelected={this.onChangeText}
					renderRow={this.renderRow.bind(this)}
					renderSeparator={null} />
				<FlatList
					style={{marginTop: 10}}
					refreshing={isGettingPokemon}
					onRefresh={() => this.componentDidMount()}
					keyExtractor={(item, idx) => `row_${idx}`}
					data={this.renderData()}
					renderItem={this.renderItem}
					onEndReachedThreshold={1}
          onEndReached={this.onLoadMore}
          ListFooterComponent={<Loading isLoading={isLoadMorePokemon}
					 color={Color.black}/>}/>
				</View>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {flex: 1, backgroundColor: "white"},

	sectionStyle: {flexDirection: "row",
		backgroundColor: Color.colorPrimary,
		marginBottom: 1, padding: 10, alignItems: "center"},
	sectionImage: {width: 30, height: 30},
	sectionText: { flex: 1, marginLeft: 10, fontWeight: "bold", fontSize: 18, color: Color.white},

	rowStyle: {flexDirection: "row", backgroundColor: Color.white,  borderBottomColor: Color.gray04, 
		borderBottomWidth: StyleSheet.hairlineWidth, paddingVertical: 5, alignItems: "center", },
	rowView: {width: 5, backgroundColor: Color.colorAccent},
	rowText: {padding: 8, fontSize: 16, color: Color.gray05},
});

export default HomeScreen;
