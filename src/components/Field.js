import React from "react";
import {View, Text, TextInput, StyleSheet, Image, 
  TouchableWithoutFeedback} from "react-native";
import PropTypes from "prop-types";
import {Icon, Avatar} from "react-native-elements";
import Color from "__color";
import Res from "__image";
import _ from "lodash";

class Field extends React.PureComponent {

  renderIcon(){
		switch (this.props.icon) {
		case "Loading":
			return (
				<View style={styles.load}>
					<Loading size="small" />
				</View>);
		case "expand":
			return <Icon name="chevron-down" type="evilicon" color={Color.text7} size={27} />;
		case "date":
			return <Icon name="date-range" size={25} color="black" />;
		case "time":
			return <Icon name="schedule" size={25} color="black" />;
		case "php":
			return <Image style={{width: 18, height: 18}} source={Res.get("PHP")} resizeMode="contain"/>;
		default:
			return null;
		}
	}

  render(){
    const {label, containerStyle, bordStyle, error, inputStyle, disable, date, 
      TouchableComponent, onChangeText, placeholder, labelStyle, value} = this.props;
    const editable = disable || date ? false : true;
    const viewStyle = [
      disable && styles.noBorder,
      error && {borderColor: Color.red}
    ];

    return (
      <TouchableComponent disabled={!date} underlayColor={"transparent"} >
        <View  style={[styles.container, containerStyle]}>
          <Text style={[styles.label, labelStyle, error && {color: Color.red}]}>{label}</Text>
          <View style={[styles.border, ...viewStyle, bordStyle]}>
            {disable ? <Text style={[styles.input, inputStyle]}>{value}</Text> 
            :
            <TextInput style={[styles.input, inputStyle]}
              autoCorrect={false}
              {...this.props}
              placeholder={placeholder}
              onChangeText={onChangeText}
              editable={editable} />
            }
            {this.renderIcon()}
          </View>
          {!_.isEmpty(error) ? <Text style={styles.txtError}>{error}</Text> : null}
        </View>
      </TouchableComponent>
    )
  }
}

Field.defaultProps = {
  TouchableComponent: TouchableWithoutFeedback,
};

Field.propTypes = {
  error: PropTypes.string
};

const styles = StyleSheet.create({
  container: {marginTop: 20},
  label: { fontWeight: "400", fontSize: 14, color: Color.text7, opacity: 0.5},
  border: {minHeight: 40, borderRadius: 5, borderBottomWidth: 0.5, borderBottomColor: Color.border2, 
    justifyContent: "space-between", alignItems: "center", marginTop: 6, flexDirection: "row"},
  noBorder: {borderBottomWidth: 0},
  input: {flex: 1,  fontSize: 14, color: Color.text7, 
    paddingVertical: 0},
  txtError: {marginTop: 5,  fontSize: 13, color: Color.red},
});

export default Field;