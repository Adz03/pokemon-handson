import React, {PureComponent} from "react";
import { StyleSheet, View, ViewPropTypes, Modal} from "react-native";
import PropTypes from "prop-types";
import {Spinner} from "native-base";
import Color from "__color";

export default class LoadingModal extends PureComponent {
	static defaultProps = {
		color: "black",
	}
	
	render() {
		const { isLoading, visible} = this.props;

		return (
			<Modal animationType="none" transparent
        onRequestClose={() => console.log()}
        visible={visible}>
				<View style={styles.tansparent}>
					<View style={[styles.container]}>
						<Spinner {...this.props}
							size="small"
							color={"black"}
							animating={isLoading} />
					</View>
				</View>
			</Modal>
		);
	}
}
LoadingModal.propTypes = {
	color: PropTypes.string,
	isLoading: PropTypes.bool,
	customStyle: ViewPropTypes.style,
};

const styles = StyleSheet.create({
	tansparent: {flex: 1, alignItems: "center", justifyContent: "center", 
		backgroundColor: 'rgba(0, 0, 0, 0.2)'},

	container: {width: 40, height: 40, borderRadius: 20, backgroundColor: Color.colorPrimary,
		alignItems: "center", justifyContent: "center"},
});
