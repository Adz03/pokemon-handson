import React from "react";
import {TouchableOpacity, StyleSheet, Text, View} from "react-native";
import Loading from "./Loading";
import Color from "__color";

const Button = (props) => {
  const {onPress, style, labelStyle, label, isLoading, color} = props;

  return (
    <>
    {isLoading ? 
    <View style={[styles.container, style]}>
      <Loading isLoading color={color || Color.colorPrimary}/>
    </View> : 
    <TouchableOpacity activeOpacity={0.8} style={[styles.container, style]} 
      onPress={onPress}>
      <Text style={[styles.labelStyle, labelStyle]}>
        {label || "PROCEED"}</Text>
    </TouchableOpacity>}
    </>
  )
}

const styles = StyleSheet.create({
  container: { height: 40, justifyContent: 'center', alignItems: 'center',
		borderRadius: 5, backgroundColor: "#000000"},
  labelStyle: {fontSize: 16, color: Color.colorPrimary}
})

export default Button;