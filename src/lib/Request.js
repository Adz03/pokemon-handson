/* eslint-disable no-throw-literal */
import Axios from "axios";
import qs from "qs";
import Constant from "../app/Constant";

class Request {
	constructor(host, apiSecret, headers) {
		this.host = host;
		this.secret = apiSecret;
		this.headers = headers;
	}

	setToken = (token) => {
		this.token = token;
	}

	setFormData = (formdata) => {
		this.formdata = formdata;
	}

	get = (route) => this._request(route, "GET");

	post = (route, body) => this._request(route, "POST", body);

	patch = (route, body) => this._request(route, "PATCH", body);

	put = (route, body) => this._request(route, "PUT", body);

	delete = (route) => this._request(route, "DELETE");

	_request = async (route, method, body) => {
		try {
			console.debug(method, this.host, route, this.formdata);
			const payload = {
				method,
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					'X-Parse-Application-Id' : Constant.appId,
					'X-Parse-Session-Token': this.token,
				},
			};
			
			if(this.headers){
				payload.headers = {
					Accept: "application/json",
  				"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
  				"User-Agent": this.headers,
  				Authorization: this.secret,
  				"Accept-Charset": "UTF-8",
  				"Content-Language": "en-US",
				}
			}

			if (method !== "GET" && method !== "HEAD") {
				if(this.headers){
					payload.body = qs.stringify(body);
				}else{
					payload.body = JSON.stringify(body);
				}
			}

			const url = `${this.host}${route}`;

			return await this._sendHttpRequest(url, payload);

		} catch (e) {
			throw e;
		}
	}

	_requestFormData = async (route, method, body) => {
		try {
			const payload = {
				method,
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					'X-Parse-Application-Id' : Constant.appId,
			 		'X-Parse-Session-Token': this.token
				},
				body
			};

			const url = `${this.host}${route}`;

			return await this._sendHttpRequest(url, payload);

		} catch (e) {
			throw e;
		}
	}

	_sendHttpRequest = async (url, payload) => {
		payload.url = url;
		console.log("REQUEST PAYLOAD");
		console.log(payload);

		const response = await fetch(url, payload);
		console.log("response", response);

		if (response.ok === false){
			throw await response.json();
		}

		return response.json().catch(() => {
			return {message: "Success"};
		});
	}
}

export default Request;
